<?php
exit;
require __DIR__. '/__connect_db.php';

$begin = microtime(true);
echo $begin. '<br>';

    $sql = "INSERT INTO `address_book`(
            `name`, `email`, `mobile`, `birthday`, `address`
            ) VALUES (
              ?, ?, ?, ?, ?
            )";

    $stmt = $pdo->prepare($sql);

    // 開始 Transaction
    $pdo->beginTransaction();

    for($i=1; $i<50; $i++) {
        $stmt->execute([
            "李小明$i",
            "ming{$i}@gmail.com",
            "0918$i",
            "1990-02-03",
            "台南市$i",
        ]);
    }

    // 提交 Transaction
    $pdo->commit();

$end = microtime(true);
echo $end. '<br>';
echo $end-$begin. '<br>';