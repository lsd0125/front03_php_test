-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- 主機: 127.0.0.1
-- 產生時間： 2019 年 03 月 13 日 11:26
-- 伺服器版本: 10.1.37-MariaDB
-- PHP 版本： 7.1.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `mytest`
--

-- --------------------------------------------------------

--
-- 資料表結構 `address_book`
--

CREATE TABLE `address_book` (
  `sid` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `address_book`
--

INSERT INTO `address_book` (`sid`, `name`, `email`, `mobile`, `birthday`, `address`) VALUES
(1, '李小明', 'test@gmail.com', '0935-123-456', '0990-12-12', '高雄市'),
(3, 'dsfgdf', 'gray_tung@yahoo.com.tw', '0918123456', '0000-00-00', 'dfgdfs'),
(10, '李小明', 'test1@gmail.com', '0935-123-456', '0990-12-12', '高雄市'),
(12, 'dfgsdf', 'gray_chen@yahoo.com.tw', '0918111111', '0000-00-00', 'sdfsdfa'),
(14, '史大巴', 'gray_2@yahoo.com.tw', '0918111111', '1990-12-12', 'rfgaewf'),
(16, '李小明2', 't2est1@gmail.com', '0935-123-456', '0990-12-12', '高雄市'),
(17, '李小明3', 't3est1@gmail.com', '0935-123-456', '0990-12-12', '高雄市'),
(18, '李小明1', 'ming1@gmail.com', '09181', '1990-02-03', '台南市1'),
(19, '李小明2', 'ming2@gmail.com', '09182', '1990-02-03', '台南市2'),
(20, '李小明3', 'ming3@gmail.com', '09183', '1990-02-03', '台南市3'),
(21, '李小明4', 'ming4@gmail.com', '09184', '1990-02-03', '台南市4'),
(22, '李小明5', 'ming5@gmail.com', '09185', '1990-02-03', '台南市5'),
(23, '李小明6', 'ming6@gmail.com', '09186', '1990-02-03', '台南市6'),
(24, '李小明7', 'ming7@gmail.com', '09187', '1990-02-03', '台南市7'),
(25, '李小明8', 'ming8@gmail.com', '09188', '1990-02-03', '台南市8'),
(26, '李小明9', 'ming9@gmail.com', '09189', '1990-02-03', '台南市9'),
(27, '李小明10', 'ming10@gmail.com', '091810', '1990-02-03', '台南市10'),
(28, '李小明11', 'ming11@gmail.com', '091811', '1990-02-03', '台南市11'),
(29, '李小明12', 'ming12@gmail.com', '091812', '1990-02-03', '台南市12'),
(30, '李小明13', 'ming13@gmail.com', '091813', '1990-02-03', '台南市13'),
(31, '李小明14', 'ming14@gmail.com', '091814', '1990-02-03', '台南市14'),
(32, '李小明15', 'ming15@gmail.com', '091815', '1990-02-03', '台南市15'),
(33, '李小明16', 'ming16@gmail.com', '091816', '1990-02-03', '台南市16'),
(34, '李小明17', 'ming17@gmail.com', '091817', '1990-02-03', '台南市17'),
(35, '李小明18', 'ming18@gmail.com', '091818', '1990-02-03', '台南市18'),
(36, '李小明19', 'ming19@gmail.com', '091819', '1990-02-03', '台南市19'),
(37, '李小明20', 'ming20@gmail.com', '091820', '1990-02-03', '台南市20'),
(38, '李小明21', 'ming21@gmail.com', '091821', '1990-02-03', '台南市21'),
(39, '李小明22', 'ming22@gmail.com', '091822', '1990-02-03', '台南市22'),
(40, '李小明23', 'ming23@gmail.com', '091823', '1990-02-03', '台南市23'),
(41, '李小明24', 'ming24@gmail.com', '091824', '1990-02-03', '台南市24'),
(42, '李小明25', 'ming25@gmail.com', '091825', '1990-02-03', '台南市25'),
(43, '李小明26', 'ming26@gmail.com', '091826', '1990-02-03', '台南市26'),
(44, '李小明27', 'ming27@gmail.com', '091827', '1990-02-03', '台南市27'),
(45, '李小明28', 'ming28@gmail.com', '091828', '1990-02-03', '台南市28'),
(46, '李小明29', 'ming29@gmail.com', '091829', '1990-02-03', '台南市29'),
(47, '李小明30', 'ming30@gmail.com', '091830', '1990-02-03', '台南市30'),
(48, '李小明31', 'ming31@gmail.com', '091831', '1990-02-03', '台南市31'),
(49, '李小明32', 'ming32@gmail.com', '091832', '1990-02-03', '台南市32'),
(50, '李小明33', 'ming33@gmail.com', '091833', '1990-02-03', '台南市33'),
(51, '李小明34', 'ming34@gmail.com', '091834', '1990-02-03', '台南市34'),
(52, '李小明35', 'ming35@gmail.com', '091835', '1990-02-03', '台南市35'),
(53, '李小明36', 'ming36@gmail.com', '091836', '1990-02-03', '台南市36'),
(54, '李小明37', 'ming37@gmail.com', '091837', '1990-02-03', '台南市37'),
(55, '李小明38', 'ming38@gmail.com', '091838', '1990-02-03', '台南市38'),
(56, '李小明39', 'ming39@gmail.com', '091839', '1990-02-03', '台南市39'),
(57, '李小明40', 'ming40@gmail.com', '091840', '1990-02-03', '台南市40'),
(58, '李小明41', 'ming41@gmail.com', '091841', '1990-02-03', '台南市41'),
(59, '李小明42', 'ming42@gmail.com', '091842', '1990-02-03', '台南市42'),
(60, '李小明43', 'ming43@gmail.com', '091843', '1990-02-03', '台南市43'),
(61, '李小明44', 'ming44@gmail.com', '091844', '1990-02-03', '台南市44'),
(62, '李小明45', 'ming4555@gmail.com', '0918451234', '1990-02-03', '台南市45'),
(64, '李小明47', 'ming47@gmail.com', '0918-479-123', '1990-02-03', '台南市47bc.k.kl'),
(65, '明48', 'ming48@gmail.com', '0918481234', '1990-02-05', '台北gf市'),
(66, '李明49', 'min55g49@gmail.com', '0918421567', '1990-02-03', '台灣台南市49');

-- --------------------------------------------------------

--
-- 資料表結構 `admins`
--

CREATE TABLE `admins` (
  `sid` int(11) NOT NULL,
  `admin_id` varchar(255) NOT NULL COMMENT '帳號',
  `password` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 已匯出資料表的索引
--

--
-- 資料表索引 `address_book`
--
ALTER TABLE `address_book`
  ADD PRIMARY KEY (`sid`),
  ADD UNIQUE KEY `email` (`email`);

--
-- 資料表索引 `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`sid`),
  ADD UNIQUE KEY `admin_id` (`admin_id`);

--
-- 在匯出的資料表使用 AUTO_INCREMENT
--

--
-- 使用資料表 AUTO_INCREMENT `address_book`
--
ALTER TABLE `address_book`
  MODIFY `sid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- 使用資料表 AUTO_INCREMENT `admins`
--
ALTER TABLE `admins`
  MODIFY `sid` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
