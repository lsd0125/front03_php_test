<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<?php

$ar1 = array(2,3,4);

$ar2 = [5,6,7];


$ar3 = array(
    'name' => 'Bill',
    'age' => 20,
    'gender' => 'male',
);

$ar4 = [
    'name' => 'Bill',
    'age' => 20,
    'gender' => 'male',
];


$ar3[] = 100;  // array push

echo '<pre>';
print_r($ar3);

var_dump($ar4);
echo '</pre>';

?>
</body>
</html>