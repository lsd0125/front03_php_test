<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<!--<pre>-->
<?php

$ar3 = array(
    'name' => 'Bill',
    'age' => 20,
    'gender' => 'male',
);
$ar3[] = 100;  // array push

foreach($ar3 as $k => $v){
    echo "$k, $v <br>";
}

echo "--------------<br>";

foreach($_GET as $k => $v){
    echo "$k, $v <br>";
}

?>
<!--</pre>-->
</body>
</html>